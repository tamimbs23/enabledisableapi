using EnableDisableApi;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();



var app = builder.Build();

// Method 1:  Disable All controller in Within Same Namespace and completely remove it
if (true)
{
    var appPartManager = app.Services.GetService<ApplicationPartManager>();
    var mockingPart = appPartManager.ApplicationParts.FirstOrDefault(part => part.Name == "EnableDisableApi");

    if (mockingPart != null)
    {
        appPartManager.ApplicationParts.Remove(mockingPart);
    }
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

// Method 2
app.Use(async (context, next) =>
{
    string controllerName = context.Request.RouteValues["controller"].ToString();
    var isDisable = bool.Parse(builder.Configuration[$"DisableControllers:{controllerName}"]);

    if (isDisable)
    {
        context.Response.StatusCode = 404;
        return;
    }
    await next(context);
});


// Method 3
app.UseEndpoints(endpoints =>
{
    endpoints.MapGet("/WeatherForecast", async (context) =>
    {
        context.Response.StatusCode = 404;
    });
    // more routing
});



//app.UseMiddleware<IgnoreRouteMiddleware>();

app.MapControllers();

app.Run();
