﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;

namespace EnableDisableApi
{
    public class MyNoActionFilterAttribute : ActionFilterAttribute
    {
        string disbleControllers = "WeatherForecast";
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var contollerName = context.RouteData.Values["controller"].ToString();
            if (disbleControllers.Equals(contollerName))
            {
                context.Result = new NotFoundResult();
            }
            else
            {
                base.OnActionExecuting(context);
            }
        }
    }
}
