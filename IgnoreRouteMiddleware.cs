﻿namespace EnableDisableApi
{
    public class IgnoreRouteMiddleware
    {
        private readonly RequestDelegate next;
        private readonly IConfiguration configuration;

        public IgnoreRouteMiddleware(RequestDelegate Next, IConfiguration Configuration)
        {
            next = Next;
            configuration = Configuration;
        }

        public async Task Invoke(HttpContext context)
        {
            string controllerName = context.Request.RouteValues["controller"].ToString();
            var isDisable = bool.Parse(configuration[$"DisableControllers:{controllerName}"]);

            if (isDisable)
            {
                context.Response.StatusCode = 404;
                return;
            }

            await next.Invoke(context);
        }
    }
}
